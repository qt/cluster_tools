from readproc cimport PHandle, openproc, closeproc, readproc
from collections import namedtuple

proc_info = namedtuple('proc_info', ['ppid', 'cputime', 'start_time',
                                     'curtime', 'rss'])

def current_procs():
    cdef PHandle *handle
    cdef int pid, ppid
    cdef double cputime, start_time, curtime
    cdef long rss

    handle = openproc()
    if handle == NULL:
        raise RuntimeError("Cannot parse /proc/")

    processes = {}
    while readproc(handle, &pid, &ppid, &cputime, &start_time,
                   &curtime, &rss):
        processes[pid] = proc_info(ppid, cputime, start_time, curtime, rss)

    closeproc(handle)

    return processes
