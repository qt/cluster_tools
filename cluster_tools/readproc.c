#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>
#include <dirent.h>

#include "readproc.h"

struct PHandle *openproc(void)
{
  struct PHandle *handle;
  FILE *file;
  unsigned long boottime = 0;
  char buffer[1024];

  file = fopen("/proc/stat", "r");
  if( !file ) return NULL;

  while(fgets(buffer, 1024, file)) {
    if( strlen(buffer) > 6) {
      if( strncmp("btime ", buffer, 5) == 0) {
	sscanf(buffer, "btime %lu", &boottime);
      }
    }
  }

  fclose(file);

  /* if somehow no boot time was found */
  if( boottime == 0) return NULL;

  handle = malloc(sizeof(struct PHandle));
  if( !handle ) return NULL;

  handle->dir = opendir("/proc");
  if( !handle->dir ) return NULL;

  handle->tics_per_sec = sysconf(_SC_CLK_TCK);
  handle->page_size = sysconf(_SC_PAGESIZE);
  handle->boottime = boottime;

  return handle;
}

void closeproc(struct PHandle *handle)
{
  if( handle ) {
    closedir(handle->dir);
    free(handle);
  }
}

int readproc(struct PHandle *handle,
	     int *pid, int *ppid, double *cputime,
	     double *start_time, double *curtime, long *rss)
{
  static char filename[80];
  static char buffer[1024];
  unsigned long _utime, _stime;
  unsigned long long _start_time;
  char *s;
  struct dirent *ent;
  struct timeval tv;
  int fd, num_read;

  while( (ent = readdir(handle->dir)) ){
    /* check if entry is a process id */
    if( *ent->d_name > '0' && *ent->d_name <= '9' ) {
      sprintf(filename, "/proc/%s/stat", ent->d_name);

      fd = open(filename, O_RDONLY);
      if( fd == -1 ) continue; /* apparently the entry was removed
				  in the meantime */

      gettimeofday(&tv, NULL);
      num_read = read(fd, buffer, 1024 - 1);
      if(num_read < 0) {
	close(fd);
	continue;
      }
      buffer[num_read] = '\0';

      sscanf(buffer, "%d", pid);
      s = strrchr(buffer, ')') + 2;
      sscanf(s, "%*c %d %*d %*d %*d %*d %*u %*u %*u %*u %*u "
	     "%lu %lu %*d %*d %*d %*d %*d %*d %llu %*u %ld",
	     ppid, &_utime, &_stime, &_start_time, rss);

      close(fd);

      *cputime = (double)(_utime + _stime)/handle->tics_per_sec;
      *start_time = ((double)_start_time/handle->tics_per_sec)+handle->boottime;
      *curtime = tv.tv_sec + tv.tv_usec*1e-6;
      *rss = (*rss) * handle->page_size;
      
      return 1;
    }
  }

  return 0;
}
