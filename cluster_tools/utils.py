import re

memunits = {'K': 1024, 'KB': 1024, 'M': 1024 ** 2, 'MB': 1024 ** 2, 
            'G': 1024 ** 3, 'GB': 1024 ** 3}

def memory_to_bytes(memstring):
    """Convert a string/number describing memory to a
    number describing memory in bytes.
    """
    if memstring is None:
        return 0

    # If a number, assume it is in bytes
    try:
        return int(memstring)
    except:
        pass

    try:
        m = re.match("\A\s*([0-9eE.]+)\s*(\w+)\s*\Z", memstring)
        memory = float(m.group(1))
        unit = m.group(2).upper()
        return int(memory * memunits[unit])
    except:
        raise ValueError("Invalid memory string")

