#! /usr/bin/env python
import sys, time, os, subprocess
import re, warnings
import zmq, socket
from progbar import ProgressBar
from cluster_daemon import ClusterDaemonConnect

# Configuration
SSH = ["ssh", "-q", "-o", "ConnectTimeout=20"]
LOAD_GRACE = 0.1                # See code.
PING_TIMEOUT = 2                # How long to wait for a ping to finish
PING_WAIT = 5                   # Minumum time to wait between pings

MAX_N_CHILDREN = 10

class Machine(object):

    class Query(object):
        def __init__(self, machine, timeout=PING_TIMEOUT):
            self.machine = machine
            self.timeout = time.time() + timeout

            self.success = False
            self.in_process = True

        def poll(self):
            if not self.in_process:
                return True

            if self.machine.soc.poll(timeout=0):
                self.result = self.machine.soc.recv_pyobj()
                self.in_process = False
                self.success = True
                self.machine.is_pinged = False

                return True
            elif time.time() > self.timeout:
                self.machine.broken = True
                self.in_process = False
                self.error = "timeout"

                return True

            return False

        def __del__(self):
            # Note: this cleanup is only necessary if a user gets a
            # Query object without ever polling it and deleting it immediately

            if self.in_process:
                if self.machine.soc.poll(timeout=(self.timeout -
                                                  time.time()) * 1000):
                    self.machine.soc.recv_pyobj()
                    self.machine.is_pinged = False
                else:
                    #no response within timeout
                    self.machine.broken = True


    class Ping(Query):
        def __init__(self, machine, timeout=PING_TIMEOUT):
            Machine.Query.__init__(self, machine, timeout)

            self.num_jobs = 0
            self.res_num = None

        def poll(self):
            if not self.in_process:
                return True

            pollrslt = Machine.Query.poll(self)

            if pollrslt and self.success :
                if not 'error' in self.result:
                    self.num_jobs = self.result['num_jobs']
                    if self.num_jobs > 0:
                        self.res_num = self.result['res_num']
                else:
                    self.machine.broken = True
                    raise RuntimeError("Unexpected response from "
                                       "cluster_daemon")

            return pollrslt


    class Registration(Query):
        def __init__(self, machine, timeout=PING_TIMEOUT):
            Machine.Query.__init__(self, machine, timeout)


        def poll(self):
            if not self.in_process:
                return True

            pollrslt = Machine.Query.poll(self)

            if pollrslt and self.success:
                if 'error' in self.result:
                    self.error = self.result['error']
                    self.success = False

            return pollrslt


    class RunCommand(object):
        def __init__(self, machine, to_be_run, commands, res_num=None):
            self.machine = machine
            self.commands = commands
            self.res_num = res_num
            self.pids = None
            self.in_process = True
            self.success = False
            self.machine.runs = True

            try:
                self.p = subprocess.Popen(SSH + [self.machine.name,
                                           "/bin/bash"],
                                          stdin=subprocess.PIPE,
                                          stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE)
                self.p.stdin.write(to_be_run)
                self.p.stdin.close()
                self.p.stdin = None

            except OSError, e:
                self.machine.broken = True
                self.in_process = False
                raise RuntimeError("Serious problem: execution of ssh "
                                   "failed: " + str(e))

        def poll(self):
            if not self.in_process:
                return True

            if self.p.poll() is not None:
                self.in_process = False
                self.machine.runs = False

                if self.p.returncode == 0:
                    outputs = self.p.communicate()[0].split()

                    if len(outputs) != len(self.commands):
                        warnings.warn("Less commands seem to have been "
                                      "executed than desired. Weird error. "
                                      "Assuming that none have started.",
                                      RuntimeWarning)
                    else:
                        self.pids = [int(pid) for pid in outputs]
                        self.commands = None
                        self.success = True
                else:
		    print self.p.communicate()
                    warnings.warn(self.machine.name + " unreachable. ssh "
                                  "returned " + str(self.p.returncode),
                                  RuntimeWarning)

                return True

            return False


    def __init__(self, name, quiet=False):
        self.name = name        # Name of machine
        self.broken = True
        self.wait_until = 0
        self.quiet = quiet

        conn = ClusterDaemonConnect(name, quiet)
        self.host = conn.host
        self.soc = conn.soc
        
        if self.soc:
            self.broken = False
            self.is_pinged = False
            self.runs = False


    def is_ready(self):
        return (not self.broken and not self.is_pinged and not self.runs and
                time.time() > self.wait_until)

    def query(self, msg):
        """
        """
        if self.is_pinged:
            raise RuntimeError("Must wait until previous query is done!")

        self.is_pinged = True

        self.soc.send_pyobj(msg)

        return self.Query(self)

    def ping(self, procs_per_job=1, mem_per_job=None, want=None):
        """
        """
        if self.is_pinged:
            raise RuntimeError("Must wait until previous query is done!")

        if procs_per_job <= 0:
            raise ValueError("procs_per_job must be positive")

        options = {'procs_per_job' : procs_per_job}
        if mem_per_job is not None:
            options['mem_per_job'] = mem_per_job
        if want is not None:
            options['want'] = want

        self.is_pinged = True
        self.wait_until = time.time() + PING_WAIT

        self.soc.send_pyobj(('query', options))

        return self.Ping(self)


    def register(self, pids, mem_per_job=None, res_num=None):
        """
        """
        if self.is_pinged:
            raise RuntimeError("Must wait until previous query is done!")

        options = {'pids' : pids}
        if mem_per_job is not None:
            options['mem_per_job'] = mem_per_job
        if res_num is not None:
            options['res_num'] = res_num
    
        self.is_pinged = True
        self.wait_until = time.time() + PING_WAIT

        self.soc.send_pyobj(('register', options))

        return self.Registration(self)


    def run(self, commands, redirect=True, header=True, res_num=None,
            quiet=None):
        """
        Run a set of commands on the cluster machine using ssh. Supports
        redirecting of stdout and stderr to file.

        Parameters:
        -----------
        commands: string or list of strings
            commands to run on the Machine. If a single string is given, this
            is interpreted as a single command.
        redirect: True or False
            Whether to redirect stdout and stderr to a file. If `False`,
            all output to stdout and stderr is discarded. Defaults to `True`.
        header: True or False
            Whether the executed command should be printed as header
            in the redirected output file. Only effective if ``redirect=True``.
            Defaults to `True`.
        quiet: True or False or None
            Whether to print status information (``quiet=False``) or
            not (``quiet=True``). Defaults to `None`, where the value
            of `quiet` passed to the `__init__`-method is taken as
            default.

        Returns:
        --------
        pids: list of integers
            The process ids of the started commands (or their parent
            shell processes)
        """

        #check if a list of commands, or a single command
        if not getattr(commands, '__iter__', False):
            commands = (commands, )

        if quiet is None:
            quiet = self.quiet

        if not redirect:
            cwd = os.getcwd().replace("'", "'\\''")
            to_be_run  = "cd '%s'\n" % cwd

            for command in commands:
                if not quiet:
                    print 'running on %s: %s' % (self.name, command)
                to_be_run += "( %s ) >/dev/null 2>/dev/null & echo $!\n" % \
                    command
        else:
            cwd = os.getcwd().replace("'", "'\\''")
            # change into working directory
            to_be_run  = "cd '%s'\n" % cwd

            for i, command in enumerate(commands):
                fname = time.strftime("%Y%m%d-%H%M%S") + '_' + self.name + \
                    "_" + str(i)
                fname.replace("'", "'\\''")

                if not quiet:
                    print '%s >>%s.dat 2>%s.err' % (command, fname, fname)

                if header:
                    to_be_run += (
                        # Create first line of output file.
                        "echo >'%s'.dat \\# Output of '%s'\n " % \
                        (fname, command.replace("'", r"'\''")))

                to_be_run += (
                        # Run command
                        "(%s;" % command +
                        # Delete error logfile if it exists and is empty.
                        "test -f '%s'.err -a ! -s '%s'.err && "
                        "rm '%s'.err) " % (3 * (fname,)) +
                        # Redirect output.
                        ">>'%s'.dat 2>'%s'.err &" % (fname, fname) +
                        # Find pid of started process (child of subshell)
                        "until cpid=$(pgrep -P $!) || ! kill -0 $!; do continue; done; test -z '$cpid' && echo '-1' || echo $cpid\n"
                        )

        return self.RunCommand(self, to_be_run, commands, res_num)


def memstr_to_kb(memory):
    m = re.match("^\s*(\d+)\s*([A-Za-z]*)", memory)

    if not m:
        raise ValueError("String cannot be interpreted as memory size")

    mem = int(m.group(1))
    unit = m.group(2).lower()

    if unit in ("", "k", "kb", "kilobyte", "kilobytes"):
        return mem
    elif unit in("m", "mb", "megabyte", "megabytes"):
        return mem * 1024
    elif unit in ("g", "gb", "gigabyte", "gigabytes"):
        return mem * 1024 * 1024
    else:
        raise ValueError("Unknown memory unit "+unit)


def schedule(mach_names, commands, n_passes=1, redirect=True,
             header=True, quiet=False, progbar=True,
             oneshot=False, pids=False, mem_per_job=None):
    """Execute a set of commands on cluster machines.

    `schedule` allows for both a queue-like mode of operation, where
    cluster machiens are polled until all commands are executed, and
    a mode of operation where every cluster machine is queried only once
    (``oneshot==True``).
    """
    if len(mach_names) == 0:
        raise ValueError("no machines specified")

    if progbar:
        quiet = True
        bar = ProgressBar("Starting on cluster", 20)
        bartime = time.time()

    if pids:
        pidlist = []

    machines = [Machine(name, quiet) for name in mach_names]
    commands = commands * n_passes
    desired_jobs = len(commands)
    safely_started = 0

    running = []
    pinging = []
    registering = []

    def _cmp(x, y):
        if x.broken == y.broken:
            return cmp(x.wait_until, y.wait_until)
        else:
            return cmp(x.broken, y.broken)

    while (len(machines) or len(pinging) or len(running) or
           len(registering)):
        done_something = False

        # sort machines with longest time since last ping first
        if not oneshot:
            machines.sort(cmp=_cmp)

        # check if ping was succesful, run commands if poosible
        to_be_removed = []
        for i, ping in enumerate(pinging):
            if ping.poll():
                done_something = True
                if not quiet:
                    print "ping on machine", ping.machine.name, "ended:", ping.num_jobs
                to_be_removed.append(i)

                if not ping.success and not quiet:
                    print "Machine", ping.machine.name, "did not respond"

                num_run = min(len(commands), ping.num_jobs)
                if num_run:
                    to_be_run = [commands.pop() for i in xrange(num_run)]
                    if not quiet:
                        print "starting commands", to_be_run
                    running.append(ping.machine.run(to_be_run,
                                                    redirect, header,
                                                    ping.res_num))

        to_be_removed.sort(reverse=True)
        for i in to_be_removed:
            del pinging[i]

        # check if ssh runs have (succesfully) finished
        to_be_removed = []
        for i, run in enumerate(running):
            if run.poll():
                done_something = True
                to_be_removed.append(i)
                if not quiet:
                    print "starting finished on ", run.machine.name,
                if run.success:
                    if progbar:
                        safely_started += len(run.pids)
                    if pids:
                        pidlist.extend(zip([run.machine.name] * len(run.pids),
                                           run.pids))

                    registering.append(run.machine.register(run.pids,
                                                            mem_per_job,
                                                            run.res_num))

                    if not quiet:
                        print "successfully"
                else:
                    if not quiet:
                        print "not good"
                    commands.extend(run.commands)

        to_be_removed.sort(reverse=True)
        for i in to_be_removed:
            del running[i]

        # check if processes were registered properly (shouldn't fail typically)
        to_be_removed = []
        for i, register in enumerate(registering):
            if register.poll():
                done_something = True
                to_be_removed.append(i)
                if not quiet:
                    print "Registration finished on machine", \
                        register.machine.name
                if not register.success:
                    print "Registration on machine", register.machine.name, \
                        "failed because of error: ", register.error
                    print "Your jobs were succesfully started, " \
                        "but counting the cpu load might be inaccurate."
                    print "Please report this error to Michael Wimmer."

        to_be_removed.sort(reverse=True)
        for i in to_be_removed:
            del registering[i]

        # ping new machines
        if not oneshot:
            for mach in machines:
                if (len(pinging) + len(running) >= MAX_N_CHILDREN or
                    len(commands) == 0):
                    break

                if mach.is_ready():
                    done_something = True
                    if not quiet:
                        print "start ping ", mach.name, "with want =",
                        print len(commands)
                    pinging.append(mach.ping(want=len(commands),
                                             mem_per_job=mem_per_job))
        else:
            while (len(pinging) + len(running) < MAX_N_CHILDREN and
                   len(commands) and len(machines)):
                mach = machines.pop()
                if mach.is_ready():
                    done_something = True
#                   print "start ping ", mach.name, "with want =", len(commands)
                    pinging.append(mach.ping(want=len(commands)))

        if not done_something:
            time.sleep(0.1)

        if progbar and bartime  + 0.1 < time.time():
            bar.update(safely_started / float(desired_jobs))
            bartime = time.time()

        # if all pings and runs have finished and no commands are left,
        # we are done.
        if len(pinging) == 0 and len(running) == 0 and len(commands) == 0 \
           and len(registering) == 0:
            break

    if progbar:
        bar.clear()

    if pids:
        return pidlist
    else:
        return desired_jobs - len(commands)
