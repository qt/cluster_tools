import sys


class ProgressBar(object):
    """Simple progress bar on the console"""

    def __init__(self, text, width):
        self.text = text
        self.width = width
        self.state = 0
        self.statussym = ['|', '/', '-', '\\']
        self.update(0)

    def update(self, percent):
        progress = int(self.width * percent)
        empty = max(self.width - progress - 1, 0)
        busy = self.width - progress - empty

        sys.stdout.write("\r{0}: [{1}{2}{3}] {4}%".format(self.text,
                                     "#" * progress,
                                     self.statussym[self.state] * busy,
                                     " " * empty, int(percent * 100)))
        sys.stdout.flush()
        self.state += 1
        if self.state > 3:
            self.state = 0

    def clear(self):
        sys.stdout.write("\r" + " " * (len(self.text) + 3 + self.width + 6) +
                         "\r")
